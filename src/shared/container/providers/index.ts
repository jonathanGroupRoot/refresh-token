import { container } from 'tsyringe';

import { DayjsDateProvider } from './DateProvider/implementantions/DayjsDateProvider';
import { IDateProvider } from './IDateProvider';

container.registerSingleton<IDateProvider>(
    'DayjsDateProvider',
    DayjsDateProvider,
);
