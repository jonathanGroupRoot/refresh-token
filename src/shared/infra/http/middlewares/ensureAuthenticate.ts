import { NextFunction, Request, Response } from 'express';
import { verify } from 'jsonwebtoken';

import auth from '@modules/accounts/config/auth';
import { UserTokenRepository } from '@modules/accounts/infra/typeorm/repositories/UserTokenRepository';
import { AppError } from '@shared/errors/AppError';

interface IPayload {
    sub: string;
}

export async function ensureAuthenticate(
    request: Request,
    response: Response,
    next: NextFunction,
) {
    const userTokenRepository = new UserTokenRepository();
    const authHeaders = request.headers.authorization;

    if (!authHeaders) {
        throw new AppError('Token is missing');
    }

    const [, token] = authHeaders.split(' ');

    try {
        const { secret_refresh_token } = auth;

        const { sub: user_id } = verify(
            token,
            secret_refresh_token,
        ) as IPayload;

        const user = await userTokenRepository.findByUserIdAndRefreshToken(
            user_id,
            token,
        );

        if (!user) {
            throw new AppError('User does not exists');
        }

        next();
    } catch (error) {
        throw new AppError('Invalid token', 401);
    }
}
