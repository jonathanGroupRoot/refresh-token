import { Router } from 'express';

import { AuthenticateUserController } from '@modules/accounts/useCases/authenticateUser/AuthenticateUserController';
import { RefreshTokenUserController } from '@modules/accounts/useCases/refreshToken/RefreshTokenUserController';

const authenticateUser = Router();
const authenticateUserController = new AuthenticateUserController();
const refreshTokenUserController = new RefreshTokenUserController();

authenticateUser.post('/', authenticateUserController.handle);
authenticateUser.post('/refresh-token', refreshTokenUserController.handle);

export { authenticateUser };
