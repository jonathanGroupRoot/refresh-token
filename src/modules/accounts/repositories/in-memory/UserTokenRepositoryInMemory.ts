import { ICreateUserTokenDTO } from '@modules/accounts/dtos/ICreateUserTokenDTO';
import { UserToken } from '@modules/accounts/infra/typeorm/entities/UserToken';

import { IUserTokenRepository } from '../IUserTokenRepository';

class UserTokenRepositoryInMemory implements IUserTokenRepository {
    userToken: UserToken[] = [];

    async create({
        user_id,
        expires_date,
        refresh_token,
    }: ICreateUserTokenDTO): Promise<UserToken> {
        const user = new UserToken();

        Object.assign(user, {
            user_id,
            expires_date,
            refresh_token,
        });

        this.userToken.push(user);

        return user;
    }

    async findByUserIdAndRefreshToken(
        user_id: string,
        refresh_token: string,
    ): Promise<UserToken> {
        return this.userToken.find(
            user =>
                user.user_id === user_id &&
                user.refresh_token === refresh_token,
        );
    }
}

export { UserTokenRepositoryInMemory };
