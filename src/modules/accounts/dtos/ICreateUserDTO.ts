interface ICreateUserDTO {
    name: string;
    email: string;
    password: string;
    isAdmin?: string;
}

export { ICreateUserDTO };
