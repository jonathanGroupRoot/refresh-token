import { getRepository, Repository } from 'typeorm';

import { ICreateUserTokenDTO } from '@modules/accounts/dtos/ICreateUserTokenDTO';
import { IUserTokenRepository } from '@modules/accounts/repositories/IUserTokenRepository';

import { UserToken } from '../entities/UserToken';

class UserTokenRepository implements IUserTokenRepository {
    private repository: Repository<UserToken>;

    constructor() {
        this.repository = getRepository(UserToken);
    }

    async create({
        user_id,
        expires_date,
        refresh_token,
    }: ICreateUserTokenDTO): Promise<UserToken> {
        const user = this.repository.create({
            user_id,
            expires_date,
            refresh_token,
        });
        this.repository.save(user);
        return user;
    }

    async findByUserIdAndRefreshToken(
        user_id: string,
        refresh_token: string,
    ): Promise<UserToken> {
        const user = this.repository.findOne({
            user_id,
            refresh_token,
        });
        return user;
    }

    async delete(id: string): Promise<void> {
        await this.repository.delete(id);
    }
}
export { UserTokenRepository };
