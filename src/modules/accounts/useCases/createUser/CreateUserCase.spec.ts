import { AppError } from '@shared/errors/AppError';

import { UserRepositoryInMemory } from '../../repositories/in-memory/UserRepositoryInMemory';
import { CreateUserUseCase } from './CreateUserUseCase';

let createUserUseCase: CreateUserUseCase;
let userRepositoryInMemory: UserRepositoryInMemory;

describe('Create User', () => {
    beforeEach(() => {
        userRepositoryInMemory = new UserRepositoryInMemory();
        createUserUseCase = new CreateUserUseCase(userRepositoryInMemory);
    });

    it('should be able to new create user', async () => {
        const users = await createUserUseCase.execute({
            name: 'Jonathan',
            email: 'Jonathangrou',
            password: '32',
        });
        expect(users).toHaveProperty('id');
        expect(users.name).toEqual('Jonathan');
        expect(users.email).toEqual('Jonathangrou');
    });

    it('not should be able to create user equal', async () => {
        await createUserUseCase.execute({
            name: 'JonathanVinicius',
            email: 'Jonathangrouproot',
            password: '322',
        });

        await expect(
            createUserUseCase.execute({
                name: 'JonathanVinicius',
                email: 'Jonathangrouproot',
                password: '322',
            }),
        ).rejects.toEqual(new AppError('User already exists'));
    });
});
