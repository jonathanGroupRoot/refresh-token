import { ICreateUserDTO } from '@modules/accounts/dtos/ICreateUserDTO';
import { UserRepositoryInMemory } from '@modules/accounts/repositories/in-memory/UserRepositoryInMemory';
import { AppError } from '@shared/errors/AppError';

import { CreateUserUseCase } from '../createUser/CreateUserUseCase';
import { AuthenticateUserUseCase } from './AuthenticateUserUseCase';

let userRepositoryInMemory: UserRepositoryInMemory;
let authenticateUserUseCase: AuthenticateUserUseCase;
let createUserUseCase: CreateUserUseCase;

describe('authenticate user', () => {
    beforeAll(() => {
        userRepositoryInMemory = new UserRepositoryInMemory();
        createUserUseCase = new CreateUserUseCase(userRepositoryInMemory);
        authenticateUserUseCase = new AuthenticateUserUseCase(
            userRepositoryInMemory,
        );
    });

    it('should be able authenticate user', async () => {
        const user: ICreateUserDTO = {
            name: 'Jonathan',
            email: 'jonathanRoot',
            password: '2121',
        };

        await createUserUseCase.execute(user);

        const authenticate = await authenticateUserUseCase.execute({
            email: user.email,
            password: user.password,
        });

        expect(authenticate).toHaveProperty('token');
        expect(authenticate.user).toHaveProperty('name');
        expect(authenticate.user).toHaveProperty('email');
    });

    it('not should be able authenticate user not found', async () => {
        await expect(
            authenticateUserUseCase.execute({
                email: 'falseEmail@gmail.com',
                password: '1212121',
            }),
        ).rejects.toEqual(new AppError('Email or password incorrect'));
    });

    it('not should be able autenthicate user email incorrect', async () => {
        const user: ICreateUserDTO = {
            name: 'jonathanVini8',
            email: 'jonathanviniciusbraz@hotmail.com',
            password: '9090',
        };

        await expect(
            authenticateUserUseCase.execute({
                email: 'saas',
                password: user.password,
            }),
        ).rejects.toEqual(new AppError('Email or password incorrect'));
    });

    it('not should be able autenthicate user password incorrect', async () => {
        const user: ICreateUserDTO = {
            name: 'jonathanVini890',
            email: 'jonathanviniciusbraz90@hotmail.com',
            password: '909009',
        };

        await expect(
            authenticateUserUseCase.execute({
                email: user.email,
                password: '219120',
            }),
        ).rejects.toEqual(new AppError('Email or password incorrect'));
    });
});
