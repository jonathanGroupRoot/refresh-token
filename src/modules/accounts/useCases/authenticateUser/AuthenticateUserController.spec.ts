import request from 'supertest';
import { Connection, createConnection } from 'typeorm';

import { app } from '@shared/infra/http/app';

let connection: Connection;

describe('authenticate user', () => {
    beforeAll(async () => {
        connection = await createConnection();
        await connection.runMigrations();
    });

    afterAll(async () => {
        await connection.dropDatabase();
        await connection.close();
    });

    it('should be able authenticate user', async () => {
        await request(app).post('/user').send({
            name: 'jonathanVinic',
            email: 'jonathanRoot',
            password: '2121',
        });

        const response = await request(app).post('/authenticate').send({
            email: 'jonathanRoot',
            password: '2121',
        });

        expect(response.status).toBe(200);
        expect(response.body.user.name).toEqual('jonathanVinic');
        expect(response.body.user.email).toEqual('jonathanRoot');
    });

    it('should be able not authenticate user does not exists', async () => {
        const response = await request(app).post('/authenticate').send({
            email: 'jonathanFalse',
            password: '2121',
        });

        expect(response.status).toBe(400);
    });
});
