import { sign, verify } from 'jsonwebtoken';
import { inject, injectable } from 'tsyringe';

import auth from '@modules/accounts/config/auth';
import { IUserTokenRepository } from '@modules/accounts/repositories/IUserTokenRepository';
import { IDateProvider } from '@shared/container/providers/IDateProvider';
import { AppError } from '@shared/errors/AppError';

interface IPayload {
    sub: string;
    email: string;
}

@injectable()
class RefreshTokenUserUseCase {
    constructor(
        @inject('UserTokenRepository')
        private userTokenRepository: IUserTokenRepository,

        @inject('DayjsDateProvider')
        private dayjsDateProvider: IDateProvider,
    ) {}

    async execute(token: string): Promise<string> {
        const {
            secret_refresh_token,
            expires_in_token,
            expires_refresh_token_days,
        } = auth;

        const { email, sub } = verify(token, secret_refresh_token) as IPayload;

        const user_id = sub;

        const userToken =
            await this.userTokenRepository.findByUserIdAndRefreshToken(
                user_id,
                token,
            );

        if (!userToken) {
            throw new AppError('Refresh Token does not exists!');
        }

        await this.userTokenRepository.delete(userToken.id);

        const refresh_token = sign({ email }, secret_refresh_token, {
            subject: sub,
            expiresIn: expires_in_token,
        });

        const expires_date = this.dayjsDateProvider.addDays(
            expires_refresh_token_days,
        );

        await this.userTokenRepository.create({
            user_id,
            refresh_token,
            expires_date,
        });
        return refresh_token;
    }
}

export { RefreshTokenUserUseCase };
