// import { sign } from 'jsonwebtoken';

// import auth from '@modules/accounts/config/auth';
// import { UserRepositoryInMemory } from '@modules/accounts/repositories/in-memory/UserRepositoryInMemory';
// import { UserTokenRepositoryInMemory } from '@modules/accounts/repositories/in-memory/UserTokenRepositoryInMemory';
// import { AppError } from '@shared/errors/AppError';

// import { CreateUserUseCase } from '../createUser/CreateUserUseCase';
// import { RefreshTokenUserUseCase } from './RefreshTokenUserUseCase';

// let userTokenRepositoryInMemory: UserTokenRepositoryInMemory;
// let refreshTokenUserUseCase: RefreshTokenUserUseCase;
// let userRepositoryInMemory: UserRepositoryInMemory;
// let createUserUseCase: CreateUserUseCase;

// describe('refresh token', () => {
//     beforeAll(() => {
//         userTokenRepositoryInMemory = new UserTokenRepositoryInMemory();
//         refreshTokenUserUseCase = new RefreshTokenUserUseCase(
//             userTokenRepositoryInMemory,
//         );

//         userRepositoryInMemory = new UserRepositoryInMemory();
//         createUserUseCase = new CreateUserUseCase(userRepositoryInMemory);
//     });

//     it('should be able to a new refresh_token user', async () => {
//         const user = await createUserUseCase.execute({
//             name: 'jonathan vinicius',
//             email: 'jonathanvinicius',
//             password: '21212',
//         });

//         await userTokenRepositoryInMemory.findByUserIdAndRefreshToken(
//             user.id,
//         );
//         const { secret_refresh_token } = auth;

//         const refresh_token = sign({}, secret_refresh_token);
//         const user_id = user.id;

//         const token = await refreshTokenUserUseCase.execute({
//             user_id,
//             refresh_token,
//             expires_date: new Date(),
//         });

//         console.log(token);
//     });

//     // it('not should be able refresh_token, not found user', async () => {
//     //     interface IUser {
//     //         user_id: string;
//     //         token: string;
//     //     }

//     //     const user: IUser = {
//     //         user_id: '21212',
//     //         token: '2121',
//     //     };

//     //     await expect(
//     //         userTokenRepositoryInMemory.findByUserIdAndRefreshToken(
//     //             user.user_id,
//     //             user.token,
//     //         ),
//     //     ).rejects.toEqual(new AppError('Refresh Token does not exists!'));
//     // });
// });
