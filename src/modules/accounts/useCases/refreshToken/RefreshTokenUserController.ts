import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { RefreshTokenUserUseCase } from './RefreshTokenUserUseCase';

class RefreshTokenUserController {
    async handle(request: Request, response: Response): Promise<Response> {
        const token =
            request.body.token ||
            request.headers['x-access-token'] ||
            request.query.token;

        const refreshTokenUseCase = container.resolve(RefreshTokenUserUseCase);

        const refreshToken = await refreshTokenUseCase.execute(token);

        return response.json(refreshToken);
    }
}

export { RefreshTokenUserController };
