import { inject, injectable } from 'tsyringe';

import { User } from '@modules/accounts/infra/typeorm/entities/User';
import { IUserRepository } from '@modules/accounts/repositories/IUserRepository';

@injectable()
class ListUserUseCase {
    constructor(
        @inject('UserRepository')
        private listUserRepository: IUserRepository,
    ) {}
    async execute(): Promise<User[]> {
        const user = await this.listUserRepository.list();
        return user;
    }
}

export { ListUserUseCase };
