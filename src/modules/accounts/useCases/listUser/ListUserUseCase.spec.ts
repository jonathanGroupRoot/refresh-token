import { hash } from 'bcryptjs';

import { UserRepositoryInMemory } from '@modules/accounts/repositories/in-memory/UserRepositoryInMemory';

import { CreateUserUseCase } from '../createUser/CreateUserUseCase';
import { ListUserUseCase } from './ListUserUseCase';

let userRepositoryInMemory: UserRepositoryInMemory;
let createUserUseCase: CreateUserUseCase;
let listUserUseCase: ListUserUseCase;

describe('list users', () => {
    beforeAll(() => {
        userRepositoryInMemory = new UserRepositoryInMemory();
        createUserUseCase = new CreateUserUseCase(userRepositoryInMemory);
        listUserUseCase = new ListUserUseCase(userRepositoryInMemory);
    });

    it('should be able list all users', async () => {
        const passwordHash = await hash('admin', 8);

        const users = await createUserUseCase.execute({
            name: 'jonathanRoot',
            email: 'rootJonathan',
            password: passwordHash,
        });

        const user = await listUserUseCase.execute();
        expect(user).toEqual([users]);
        expect(user.length).toBe(1);
    });
});
